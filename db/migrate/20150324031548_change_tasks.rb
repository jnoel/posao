class ChangeTasks < ActiveRecord::Migration
  def self.up
  	change_table :tasks do |t|
	  	t.string :technician_validate
		end
  end

  def self.down
  	change_table :tasks do |t|
	 		t.remove :technician_validate
		end
  end
end
