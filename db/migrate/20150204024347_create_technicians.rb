class CreateTechnicians < ActiveRecord::Migration
  def up
  	create_table :technicians do |t|
  		t.string :name, null: false
  		t.string :login, null: false
  		t.string :salt
  		t.string :passwordhash
  		t.string :email
  		t.boolean :admin, default: false
  		t.boolean :rapporteur, default: false
  		t.boolean :actif, default: true
  		t.timestamps null: false
  	end
  	
  	create_table :technicians_types do |t|
  		t.integer :technician_id
  		t.integer :type_id
  		t.timestamps null: false
  	end
  	
  	create_table :technicians_sites do |t|
  		t.integer :technician_id
  		t.integer :site_id
  		t.timestamps null: false
  	end
  	
  	salt = BCrypt::Engine.generate_salt
		passwordhash = BCrypt::Engine.hash_secret("admin", salt) 
  	tech = Technician.create name: "Administrateur", login: "admin", salt: salt, passwordhash: passwordhash, admin: true, rapporteur: true
  	Type.all.each do |type|
  		Technicians_type.create technician_id: tech.id, type_id: type.id
  	end
  	Site.all.each do |site|
  		Technicians_site.create technician_id: tech.id, site_id: site.id
  	end
  	
  	salt = BCrypt::Engine.generate_salt
		passwordhash = BCrypt::Engine.hash_secret("test", salt) 
  	tech = Technician.create name: "Tech1", login: "tech1", salt: salt, passwordhash: passwordhash
  	[1,2,4,5].each do |type|
  		Technicians_type.create technician_id: tech.id, type_id: type
  	end
  	Site.all.each do |site|
  		Technicians_site.create technician_id: tech.id, site_id: site.id
  	end
  end
  
  def down
  	drop_table :technicians
  	drop_table :technicians_types
  	drop_table :technicians_sites
  end
end
