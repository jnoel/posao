class CreateTasks < ActiveRecord::Migration
  def up
  	create_table :tasks do |t|
  		t.text :description, null: false
  		t.text :recommendation
  		t.belongs_to :place, index: true
  		t.belongs_to :type, index: true
  		t.belongs_to :state, index: true
  		t.belongs_to :technician, index: true
  		t.boolean :new, default: false
  		t.date :date, null: false
  		t.date :date_echeance
  		t.timestamps null: false
  	end
  	
  	tasks = YAML.load_file('./db/migrate/example.yml')["tasks"]
  	tasks.each do |id, task|
  		tech = (task["tech"] ? Technician.find(task["tech"]) : nil)
  		Task.create description: task["description"], place: Place.find(task["lieu"]), type: Type.find(task["type"]), technician: tech, state:  State.find(task["status"]), date: Date.parse(task["date"]), date_echeance: (task["date_echeance"] ? Date.parse(task["date_echeance"]) : nil)
  	end
  end
  
  def down
  	drop_table :tasks
  end
end
