class CreateActions < ActiveRecord::Migration
  def up
  	create_table :actions do |t|
  		t.string :description, null: false
  		t.belongs_to :task, index: true
  		t.belongs_to :technician, index: true
  		t.belongs_to :supplier, index: true
  		t.date :date, null: false
  		t.timestamps null: false
  	end
  end
  
  def down
  	drop_table :actions
  end
end
