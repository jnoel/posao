class CreateSuppliers < ActiveRecord::Migration
  def up
  	create_table :suppliers do |t|
  		t.string :name, null: false
  		t.boolean :active, default: true
  		t.timestamps null: false
  	end
  	
  	suppliers = YAML.load_file('./db/migrate/example.yml')["suppliers"]
  	suppliers.each do |supplier|
  		Supplier.create name: supplier
  	end
  end
  
  def down
  	drop_table :suppliers
  end
end
