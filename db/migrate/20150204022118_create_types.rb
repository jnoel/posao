class CreateTypes < ActiveRecord::Migration
  def up
  	create_table :types do |t|
  		t.string :description, null: false
  		t.string :icone, null: false
  		t.string :icone_plan, null: false
  		t.timestamps null: false
  	end
  	
  	types = YAML.load_file('./db/migrate/example.yml')["types"]
  	types.each do |id, type|
  		Type.create description: type["description"], icone: type["icone"], icone_plan: type["icone_plan"]
  	end
  end
  
  def down
  	drop_table :types
  end
end
