class CreatePlaces < ActiveRecord::Migration
  def up
  	create_table :places do |t|
  		t.string :name, null: false
  		t.string :description
  		t.belongs_to :site, index: true
  		t.integer :x
  		t.integer :y
  		t.timestamps null: false
  	end
		
		 places = YAML.load_file('./db/migrate/example.yml')["places"]
		 places.each do |id, place|
		 	site = Site.find(place["site"])
		 	Place.create name: place["description"], site: site, x: place["x"], y: place["y"]
		 end
  end
  
  def down
  	drop_table :places
  end
end
