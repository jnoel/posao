class ChangePlaces < ActiveRecord::Migration
  def up
  	change_column :places, :x, :float
  	change_column :places, :y, :float
  end
end
