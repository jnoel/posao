class CreateStates < ActiveRecord::Migration
  def up
  	create_table :states do |t|
  		t.string :name, null: false
  		t.timestamps null: false
  	end
  	
  	states = YAML.load_file('./db/migrate/example.yml')["states"]
  	states.each do |id, state|
  		State.create name: state
  	end
  end
  
  def down
  	drop_table :states
  end
end
