class CreateSites < ActiveRecord::Migration
  def up
  	create_table :sites do |t|
  		t.string :name, null: false
  		t.string :plan
  	end
  	Site.create name: "Maison", plan:"plan1.png"
  	Site.create name: "Bungalow", plan:"plan2.png"
  end
  
  def down
  	drop_table :sites
  end
end
