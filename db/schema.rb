# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161128210912) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "actions", force: :cascade do |t|
    t.string   "description",   null: false
    t.integer  "task_id"
    t.integer  "technician_id"
    t.integer  "supplier_id"
    t.date     "date",          null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "actions", ["supplier_id"], name: "index_actions_on_supplier_id", using: :btree
  add_index "actions", ["task_id"], name: "index_actions_on_task_id", using: :btree
  add_index "actions", ["technician_id"], name: "index_actions_on_technician_id", using: :btree

  create_table "places", force: :cascade do |t|
    t.string   "name",        null: false
    t.string   "description"
    t.integer  "site_id"
    t.float    "x"
    t.float    "y"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "num"
  end

  add_index "places", ["site_id"], name: "index_places_on_site_id", using: :btree

  create_table "sites", force: :cascade do |t|
    t.string "name", null: false
    t.string "plan"
  end

  create_table "states", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "suppliers", force: :cascade do |t|
    t.string   "name",                      null: false
    t.boolean  "active",     default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "tasks", force: :cascade do |t|
    t.text     "description",                         null: false
    t.text     "recommendation"
    t.integer  "place_id"
    t.integer  "type_id"
    t.integer  "state_id"
    t.integer  "technician_id"
    t.boolean  "new",                 default: false
    t.date     "date",                                null: false
    t.date     "date_echeance"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "technician_validate"
    t.string   "ficheA"
  end

  add_index "tasks", ["place_id"], name: "index_tasks_on_place_id", using: :btree
  add_index "tasks", ["state_id"], name: "index_tasks_on_state_id", using: :btree
  add_index "tasks", ["technician_id"], name: "index_tasks_on_technician_id", using: :btree
  add_index "tasks", ["type_id"], name: "index_tasks_on_type_id", using: :btree

  create_table "technicians", force: :cascade do |t|
    t.string   "name",                         null: false
    t.string   "login",                        null: false
    t.string   "salt"
    t.string   "passwordhash"
    t.string   "email"
    t.boolean  "admin",        default: false
    t.boolean  "rapporteur",   default: false
    t.boolean  "actif",        default: true
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "technicians_sites", force: :cascade do |t|
    t.integer  "technician_id"
    t.integer  "site_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "technicians_types", force: :cascade do |t|
    t.integer  "technician_id"
    t.integer  "type_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "types", force: :cascade do |t|
    t.string   "description", null: false
    t.string   "icone",       null: false
    t.string   "icone_plan",  null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

end
