#!/usr/bin/env ruby
# encoding: UTF-8

# Copyright (c) 2015 Jean-Noël Rouchon <mail@mithrilre>
#
# This file is part of Posao.
#
# Posao is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Posao is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Posao.  If not, see <http://www.gnu.org/licenses/>.

require 'bundler/setup'
require 'sinatra'
require 'yaml'
require 'haml'
require 'coffee-script'
require 'bcrypt'
require 'sinatra/activerecord'
require 'chartkick'
require 'groupdate'
require './helpers.rb'
require './models.rb'

configure do
  enable :sessions
  #register Barista::Integration::Sinatra
end

get '/' do
  redirect '/travaux'
end

get '/login/form' do
  haml :login_form
end

post '/login/attempt' do
	if connexion? params[:username], params[:password]
		where_user_came_from = session[:previous_url] || '/'
		redirect to where_user_came_from
	else
		session[:error] = 'Login ou mot de passe incorrects'
		redirect 'login/form'
	end
end

get '/logout' do
  session.delete(:identity)
  session[:error] = 'Vous avez été déconnecté'
  redirect "/"
end

get '/travaux' do
	authentification?
	@travaux = Task.includes(:type, :technician, :state).includes(place: [:site]).where(type_id: session[:type], state_id: session[:status], "places.site_id" => session[:site]).order("date DESC")
	erb :travaux
end

get '/travaux/:id' do
	authentification?
	@travail = nil
	@actions = []
	begin
		@travail = Task.includes(:type, :technician, :state).includes(place: [:site]).find(params[:id])
		@actions = Action.includes(:supplier, :technician).where(task_id: @travail.id).order("date DESC")
	rescue
	end
	@lieu = session[:lieu]
	session[:lieu] = nil
	@new = @travail.nil?
	haml :travail
end

post '/travaux' do
	authentification?
	travail = nil
	begin
		travail = Task.find(params[:id].to_i)
	rescue
	end
	if !travail
		travail = Task.new
		travail.technician_id = session[:identity][:id]
	end
	travail.description = params[:inputTitle]
	travail.date = Date.today
	travail.type_id = params[:selectType]
	travail.place_id = params[:selectPlace]
	travail.state_id = params[:selectStatus]
	travail.recommendation = params[:inputRecommendation]
	travail.ficheA = params[:inputFicheA]
	if params[:inputDateEch].empty?
		travail.date_echeance = nil
	else
		begin
			travail.date_echeance = Date.parse(params[:inputDateEch])
		rescue
			session[:error] = "La date d'échéance n'a pas été enregistrée"
		end
	end
	travail.technician_validate = session[:identity][:nom] if [4,5].include?(travail.state_id)
	travail.save
	p travail
	redirect "/travaux/#{travail.id}"
end

post '/action' do
	authentification?
	if params["actionId"].to_i>0
		action = Action.find(params["actionId"])
	else
		action = Action.new
	end
	action.description = params[:inputActionDescription]
	date = Date.today
	begin
		date = Date.parse(params[:inputDateAction])
	rescue
		date = Date.today
	end
	action.date = date
	action.technician_id = session[:identity][:id]
	action.task_id = params[:taskId]
	action.supplier_id = params[:selectSupplier]
	action.save
	redirect "/travaux/#{action.task_id}"
end

get '/configuration' do
	authentification?
  haml :configuration
end

get '/sites' do
	authentification?
	@sites = Site.order(:id)
  haml :sites
end

get '/sites/:id' do
	authentification?
	begin
		@site = Site.find(params[:id])
	rescue
		@site = nil
	end
  haml :site
end

post '/sites' do
	authentification?
	begin
		@site = Site.find(params[:id])
	rescue
		@site = Site.new
	end
	@site.name = params[:inputNom]
	@site.plan = params[:imagePlan]
	@site.save
	redirect "/sites"
end

get '/plan' do
	authentification?
	session[:status] = [1,2,3] unless (session[:status]==[3])
	session[:site] = [session[:site][0]] if (session[:site].count>1)
	@site = Site.find(session[:site]).first
	@lieux = preparation_lieux
  haml :plan
end

post '/filtre' do
	authentification?
	session[:type] = []
	tous_type.each do |id|
		session[:type] << id if (params["type"] || []).include?(id.to_s)
	end
	state = []
	params["state"].split("-").each do |s|
		state << s.to_i
	end
	session[:status] = state
	unless back.include?("/lieu")
		session[:site] = []
		params["site"].split("-").each do |site|
		  session[:site] << site.to_i
		end
	end

	redirect back
end

get '/statistique' do
	authentification?
  year = Date.today.year
  redirect "/statistique/#{year}"
end

get '/statistique/:year' do
	authentification?
  @year = params['year'].to_i>2000 ? params['year'].to_i : Date.today.year
  @travaux_type = Task.joins(:type, :place).where(type_id: tous_type, "places.site_id" => tous_sites).where('extract(year  from date) = ?', @year).group("types.description").count
  @travaux_type_tab = []
  @travaux_type.each { |key, value|
    @travaux_type_tab << [key, value]
  }
  @travaux_etat = Task.joins(:state, :place).where(type_id: tous_type, "places.site_id" => tous_sites).where('extract(year  from date) = ?', @year).group("states.name").count
  @travaux_etat_tab = []
  @travaux_etat.each { |key, value|
    @travaux_etat_tab << [key, value]
  }
  @travaux_site = Task.joins(place: [:site]).where(type_id: tous_type, "places.site_id" => tous_sites).where('extract(year  from date) = ?', @year).group("sites.name").count
  @travaux_site_tab = []
  @travaux_site.each { |key, value|
    @travaux_site_tab << [key, value]
  }
  @travaux_rapporteur = Task.joins(:technician, :place).where(type_id: tous_type, "places.site_id" => tous_sites).where('extract(year  from date) = ?', @year).group("technicians.name").count
  @travaux_rapporteur_tab = []
  @travaux_rapporteur.each { |key, value|
    @travaux_rapporteur_tab << [key, value]
  }
  @actions_fournisseur = Action.joins(:supplier).where('extract(year  from date) = ?', @year).group("suppliers.name").count
  @actions_fournisseur_tab = []
  @actions_fournisseur.each { |key, value|
    @actions_fournisseur_tab << [key, value]
  }
  @travaux_semaine = Task.where('extract(year  from date) = ?', @year).group_by_week(:date).count
  @travaux_semaine_tab = []
  @travaux_semaine.each { |key, value|
    @travaux_semaine_tab << [key, value]
  }
  erb :statistique
end

get '/lieu/:id' do
	authentification?
	@lieu = Place.includes(:site).find(params[:id])
	@travaux = Task.includes(:type, :technician, :state).includes(place: [:site]).where(type_id: session[:type], state_id: session[:status], place_id: params[:id].to_i)
  haml :lieu
end

get '/lieu/:id/new' do
	authentification?
	session[:lieu] = params[:id].to_i
	redirect '/travaux/0'
end

post '/lieux' do
	authentification?
	lieu = nil
	if params["lieuId"].to_i>0
		lieu = Place.find(params["lieuId"])
	else
		lieu = Place.new
		lieu.site_id = params["siteId"].to_i
	end
	lieu.name = params["inputLieuNom"]
	lieu.num = params["inputNum"]
	lieu.x = params["inputX"].to_f
	lieu.y = params["inputY"].to_f
	lieu.save
	redirect "/sites/#{params['siteId']}"
end

get '/users' do
	authentification?
	@users = Technician.order(:name)
	haml :users
end

post '/users' do
	authentification?
	user = nil
	if params["userId"].to_i>0
		user = Technician.find(params["userId"])
	else
		user = Technician.new
	end
	user.name = params["userName"]
	user.login = params["userLogin"]
	user.email = params["userEmail"]
	user.admin = params["userAdmin"]
	user.actif = params["userActif"]
	unless params["userPassword"].empty?
		salt = BCrypt::Engine.generate_salt
		passwordhash = BCrypt::Engine.hash_secret(params["userPassword"], salt)
  	user.salt = salt
  	user.passwordhash = passwordhash
	end
	user.save
	user.add_types params["typeDroits"].split(",")
	user.add_sites params["siteDroits"].split(",")
	redirect "/users"
end
