#!/usr/bin/env ruby
# encoding: UTF-8

# Copyright (c) 2015 Jean-Noël Rouchon <mail@mithrilre>
#
# This file is part of Posao.
#
# Posao is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Posao is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Posao.  If not, see <http://www.gnu.org/licenses/>.

require './posao'
require 'csv'

def import_suppliers csv
  CSV.foreach(csv, {:col_sep => ","}) {|ligne|
    Supplier.create name: ligne[0] if ligne[0]
  }
end

import_suppliers "suppliers.csv"
