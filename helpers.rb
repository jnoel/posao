# encoding: UTF-8

# Copyright (c) 2015 Jean-Noël Rouchon <mail@mithrilre>
#
# This file is part of Posao.
#
# Posao is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Posao is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Posao.  If not, see <http://www.gnu.org/licenses/>.

def username
  session[:identity] ? session[:identity][:nom] : 'Bonjour invité'
end

def authentification?
	if !session[:identity] then
	  session[:previous_url] = request.path
	  session[:error] = 'Vous devez être authentifié pour visiter cette page'
	  halt haml(:login_form)
	end
end

def admin?
	if session[:identity]
		return session[:identity][:admin]
	else
		return false
	end
end

def connexion? login, password
	tech = Technician.where(login: login).first
	if tech
		if tech.passwordhash == BCrypt::Engine.hash_secret(password, tech.salt)
			session[:identity] = {}
			session[:identity][:id] = tech.id
			session[:identity][:nom] = tech.name
			session[:identity][:admin] = tech.admin
			session[:identity][:type] = []
			Technicians_type.where(technician_id: tech.id).each do |type|
				session[:identity][:type] << type.type_id
			end
			session[:identity][:site] = []
			Technicians_site.where(technician_id: tech.id).order(:site_id).each do |site|
				session[:identity][:site] << site.site_id
			end
			session[:type] = session[:identity][:type]
			session[:site] = [session[:identity][:site][0]]
			session[:status] = [1, 2, 3]
		end
	end
	
	return !session[:identity].nil?
end

def menus
	return YAML.load_file('./config/menu.yml')["menu"]
end

def tous_type just_id=true
	if just_id
		return session[:identity][:type]
	else
		return Type.find(session[:identity][:type])
	end
end

def tous_sites just_id=true
	if just_id
		return session[:identity][:site]
	else
		return Site.find(session[:identity][:site])
	end 
end

def filtre_states
	result = [
							{
								id: "1-2-3",
								nom: "Non Résolus",
								actif: (session[:status]==[1,2,3])
							},
							{
								id: "3",
								nom: "En attente",
								actif: (session[:status]==[3])
							}]
	result += [
							{
								id: "4",
								nom: "Résolu",
								actif: (session[:status]==[4])
							},
							{
								id: "1-2-3-4-5",
								nom: "Tous",
								actif: (session[:status]==[1,2,3,4,5])
							}
					 ] unless request.path_info.include?("/plan")
	return result
end

def filtre_sites
	tous = []
	result = []
	Site.all.each do |site|
		result << { 
								id: site.id.to_s,
								nom: site.name,
								actif: (session[:site]==[site.id])
							}
		tous << site.id
	end
	result << 	{
								id: tous.join("-"),
								nom: "Tout site",
								actif: (session[:site]==tous)
							} unless request.path_info.include?("/plan")
							
	return result
end

def preparation_lieux coche_ok=true
	largeur = 1100
	result = {}
	travaux = Task.joins(:place).where(type_id: session[:type], state_id: session[:status], "places.site_id" => session[:site])
	travaux.each do |travail|
		lieu_id = travail.place.id
		if result[lieu_id]
			result[lieu_id][:icone] = "exclamation_warning.png"
		else
			place = travail.place
			lieu = {lieu_id => {
															description: place.description,
															x: place.x*largeur-30,
															y: place.y*largeur-30,
															icone: travail.type.icone_plan,
															bulle: "#{place.num} - #{place.name}",
															taille: 60,
								         }
						 }		
			result.merge! lieu
		end
		result[lieu_id][:bulle] += "\n#{travail.type.description}: #{travail.description}"
	end
	
	if coche_ok
	  Place.where(site_id: session[:site]).each do |lieu|
		  unless result[lieu.id]
			  lieu = {lieu.id => {
														  nom: lieu.name,
														  description: lieu.description,
														  x: lieu.x*largeur-5,
														  y: lieu.y*largeur-5,
														  icone: "check.png",
														  bulle: "#{lieu.num} - #{lieu.name}",
															taille: 10,
					             		 }
							  }
			  result.merge! lieu
		  end
	  end
	end
	
	return result
	
end
