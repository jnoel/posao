# coding: utf-8

# Copyright (c) 2015 Jean-Noël Rouchon <mail@mithrilre>
#
# This file is part of Posao.
#
# Posao is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Posao is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Posao.  If not, see <http://www.gnu.org/licenses/>.

class Technician < ActiveRecord::Base
	validates_presence_of :name, :login
	has_many :tasks
	has_many :technicians_rights
	
	def get_types
		return Technicians_type.includes(:type).where(:technician_id => self.id)
	end
	
	def get_types_ids
		ids = []
		Technicians_type.includes(:type).where(:technician_id => self.id).each do |type|
			ids << type.type_id
		end
		return ids
	end
	
	def add_types ids
		Technicians_type.where(:technician_id => self.id).destroy_all
		ids.uniq.each do |id|
			Technicians_type.create :technician_id => self.id, :type_id => id
		end
	end
	
	def get_sites
		return Technicians_site.includes(:site).where(:technician_id => self.id)
	end
	
	def get_sites_ids
		ids = []
		Technicians_site.includes(:site).where(:technician_id => self.id).each do |site|
			ids << site.site_id
		end
		return ids
	end
	
	def add_sites ids
		Technicians_site.where(:technician_id => self.id).destroy_all
		ids.uniq.each do |id|
			Technicians_site.create :technician_id => self.id, :site_id => id
		end
	end
end

class Technicians_type < ActiveRecord::Base
	belongs_to :technician
	belongs_to :type
end

class Technicians_site < ActiveRecord::Base
	belongs_to :technician
	belongs_to :site
end

class State < ActiveRecord::Base
	validates_presence_of :name
	has_many :tasks
end

class Type < ActiveRecord::Base
	validates_presence_of :description, :icone, :icone_plan
	has_many :tasks
end

class Site < ActiveRecord::Base
	validates_presence_of :name
	has_many :places
end

class Place < ActiveRecord::Base
	validates_presence_of :name
	belongs_to :site
	has_many :tasks
end

class Task < ActiveRecord::Base
	validates_presence_of :description, :place_id, :type_id, :state_id, :date
	belongs_to :technician
	belongs_to :type
	belongs_to :place
	belongs_to :state
	has_many :actions
end

class Supplier < ActiveRecord::Base
	validates_presence_of :name
	has_many :actions
end

class Action < ActiveRecord::Base
	validates_presence_of :description
	belongs_to :supplier
	belongs_to :technician
	belongs_to :task
end
